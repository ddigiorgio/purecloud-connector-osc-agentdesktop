﻿using System;

namespace AppControl
{
    public class CopiedStringEventArgs: EventArgs
    {
        public string CopiedString { get; set; }
    }
}
