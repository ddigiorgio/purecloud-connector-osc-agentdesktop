using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using System.IO;
using System.Text;

namespace AppControl
{

	/// <summary>
	/// Application Display Control
	/// </summary>
	[
	ToolboxBitmap(typeof(ApplicationControl), "AppControl.bmp"),	
	]
	public class ApplicationControl : System.Windows.Forms.Panel
	{
        public event EventHandler<CopiedStringEventArgs> CopiedStringEvent;
        
        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, ref COPYDATASTRUCT lParam);

        public struct COPYDATASTRUCT
        {
            public IntPtr dwData;
            public int cbData;
            [MarshalAs(UnmanagedType.LPStr)]
            public string lpData;
        }

        /// <summary>
        /// Track if the application has been created
        /// </summary>
        bool created = false;

        private Process process;
        
		/// <summary>
		/// Handle to the application Window
		/// </summary>
		IntPtr appWin;

        private string arguments = "";
		public string Arguments
        {
            get
            {
                return arguments;
            }
            set
            {
                arguments = value;
            }
        }

        private string exeLocation = "";
        public string EXELocation
        {
            get
            {
                return exeLocation;
            }
            set
            {
                exeLocation = value;
            }
        }


        /// <summary>
        /// Constructor
        /// </summary>
        public ApplicationControl()
		{			
		}


		[DllImport("user32.dll", EntryPoint="GetWindowThreadProcessId",  SetLastError=true,
			 CharSet=CharSet.Unicode, ExactSpelling=true,
			 CallingConvention=CallingConvention.StdCall)]
		private static extern long GetWindowThreadProcessId(long hWnd, long lpdwProcessId); 
			
		[DllImport("user32.dll", SetLastError=true)]
		private static extern IntPtr FindWindow (string lpClassName, string lpWindowName);

		[DllImport("user32.dll", SetLastError=true)]
		private static extern long SetParent (IntPtr hWndChild, IntPtr hWndNewParent);

		[DllImport("user32.dll", EntryPoint="GetWindowLongA", SetLastError=true)]
		private static extern long GetWindowLong (IntPtr hwnd, int nIndex);

		[DllImport("user32.dll", EntryPoint="SetWindowLongA", SetLastError=true)]
		private static extern long SetWindowLong (IntPtr hwnd, int nIndex, long dwNewLong);

		[DllImport("user32.dll", SetLastError=true)]
		private static extern long SetWindowPos(IntPtr hwnd, long hWndInsertAfter, long x, long y, long cx, long cy, long wFlags);
		
		[DllImport("user32.dll", SetLastError=true)]
		private static extern bool MoveWindow(IntPtr hwnd, int x, int y, int cx, int cy, bool repaint);
		
		[DllImport("user32.dll", EntryPoint="PostMessageA", SetLastError=true)]		
		private static extern bool PostMessage(IntPtr hwnd, uint Msg, long wParam, long lParam);

        [DllImport("user32.dll", SetLastError=true, CharSet=CharSet.Auto)]
        private static extern uint RegisterWindowMessage(string lpString);

        private const int SWP_NOOWNERZORDER = 0x200;
		private const int SWP_NOREDRAW = 0x8;
		private const int SWP_NOZORDER = 0x4;
		private const int SWP_SHOWWINDOW = 0x0040;
		private const int WS_EX_MDICHILD = 0x40;
		private const int SWP_FRAMECHANGED = 0x20;
		private const int SWP_NOACTIVATE = 0x10;
		private const int SWP_ASYNCWINDOWPOS = 0x4000;
		private const int SWP_NOMOVE = 0x2;
		private const int SWP_NOSIZE = 0x1;
		private const int GWL_STYLE = (-16);
		private const int WS_VISIBLE = 0x10000000;
		private const int WM_CLOSE = 0x10;
		private const int WS_CHILD = 0x40000000;
        public const int WM_COPYDATA = 0x4A;

        [StructLayout(LayoutKind.Sequential)]
        struct COPYDATASTRUCTR
        {
            public IntPtr dwData;
            public int cbData;
            public IntPtr lpData;
        }

        /// <summary>
        /// Force redraw of control when size changes
        /// </summary>
        /// <param name="e">Not used</param>
        protected override void OnSizeChanged(EventArgs e)
		{
			this.Invalidate();
			base.OnSizeChanged(e);
		}


        public void Init()
        {
            OnVisibleChanged(null);
        }

        public void Close()
        {
            /*var pList = System.Diagnostics.Process.GetProcessesByName("ChromiumFXWebBrowser");
            foreach (Process p in pList)
            {
                p.Close();
            }*/

            if (process != null)
                process.Close();
        }

		/// <summary>
		/// Creeate control when visibility changes
		/// </summary>
		/// <param name="e">Not used</param>
		protected override void OnVisibleChanged(EventArgs e)
		{

			// If control needs to be initialized/created
			if (created == false)
			{
                string exePathFile = this.exeLocation;
                if (!File.Exists(exePathFile))
                    throw new FileNotFoundException("File not found: " + exePathFile);

				// Mark that control is created
				created = true;

				// Initialize handle value to invalid
				appWin = IntPtr.Zero;

				// Start the remote application
				
				try
				{
                    this.arguments += " " + this.Handle.ToInt32();
                    // Start the process                    
                    process = System.Diagnostics.Process.Start(exePathFile, this.arguments);

                  

                    // Wait for process to be created and enter idle condition
                    process.WaitForInputIdle();

                    while (process.MainWindowHandle == IntPtr.Zero)
                    {

                        //Thread.Sleep(1000);

                        // Get the main handle
                        
                        process.Refresh();
                        Thread.Sleep(50);
                    }

                    appWin = process.MainWindowHandle;
                }
				catch (Exception ex)
				{
					MessageBox.Show(this, ex.Message, "Error");
				}			
            
				// Put it into this form
				SetParent(appWin, this.Handle);

				// Remove border and whatnot
				SetWindowLong(appWin, GWL_STYLE, WS_VISIBLE);

				// Move the window to overlay it on this window
				MoveWindow(appWin, 0, 0, this.Width, this.Height, true);
				
			}

			base.OnVisibleChanged (e);
		}

	
		/// <summary>
		/// 
		/// </summary>
		/// <param name="e"></param>
		protected override void OnHandleDestroyed(EventArgs e)
		{
			// Stop the application
			if (appWin != IntPtr.Zero)
			{

				// Post a colse message
				PostMessage(appWin, WM_CLOSE, 0, 0);

				// Delay for it to get the message
				System.Threading.Thread.Sleep(1000);

				// Clear internal handle
				appWin = IntPtr.Zero;

			}

			base.OnHandleDestroyed (e);
		}


		/// <summary>
		/// Update display of the executable
		/// </summary>
		/// <param name="e">Not used</param>
		protected override void OnResize(EventArgs e)
		{
			if (this.appWin != IntPtr.Zero)
			{
				MoveWindow(appWin, 0, 0, this.Width, this.Height, true);
			}
			base.OnResize (e);
		}

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_COPYDATA)
            {
                COPYDATASTRUCT mystr = new COPYDATASTRUCT();
                Type mytype = mystr.GetType();
                mystr = (COPYDATASTRUCT)m.GetLParam(mytype);

                EventHandler<CopiedStringEventArgs> copyHandler = this.CopiedStringEvent;
                
                if (copyHandler != null)
                {
                    copyHandler.BeginInvoke(this, new CopiedStringEventArgs()
                    {
                        CopiedString = mystr.lpData                        
                    }, null, null);
                }
            }
            else
            {
                base.WndProc(ref m);
            }
        }

        public void sendMessageToWidget(string msg)
        {
            //string msg = JsonConvert.SerializeObject(message);
            byte[] sarr = System.Text.Encoding.Default.GetBytes(msg);
            int len = sarr.Length;
            COPYDATASTRUCT cds;
            cds.dwData = (IntPtr)100;
            cds.lpData = msg;
            cds.cbData = len + 1;
            SendMessage(this.process.MainWindowHandle, WM_COPYDATA, 0, ref cds);
        }

       
    }


}
