﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppControl
{
    public sealed class PureCloudConnectorFacade
    {
        private static AppControl.ApplicationControl instance = null;
        private static readonly object padlock = new object();

        public static AppControl.ApplicationControl Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new ApplicationControl();
                    }
                    return instance;
                }
            }
        }


    }
}
