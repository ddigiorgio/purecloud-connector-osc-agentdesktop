﻿using Chromium.WebBrowser;
using System.Windows.Forms;

namespace ChromiumInteraction
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private ChromiumWebBrowser wb;
        private string purecloudUrl;
        private string interactionId;


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitArguments()
        {
            if (this.args.Length != 2)
                throw new System.Exception("Expected 2 parameters instead of " + this.args.Length);

            this.purecloudUrl = this.args[0];
            this.interactionId = this.args[1];
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            InitArguments();
            this.components = new System.ComponentModel.Container();
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Text = "Form1";
            wb = new ChromiumWebBrowser(string.Format("{0}/directory/#/interaction/{1}", this.purecloudUrl, this.interactionId));
            

            wb.RequestHandler.CanSetCookie += (s, e) =>
            {
                e.SetReturnValue(true);
            };

            wb.RequestHandler.CanGetCookies += (s, e) =>
            {
                e.SetReturnValue(true);
            };
            
            wb.Dock = DockStyle.Fill;

            this.Controls.Add(wb);
        }

        #endregion
    }
}

