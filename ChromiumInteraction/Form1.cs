﻿using Chromium.WebBrowser;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChromiumInteraction
{
    public partial class Form1 : Form
    {
        private string[] args;
        public Form1(string[] args)
        {
            this.args = args;
            ChromiumWebBrowser.OnBeforeCommandLineProcessing += (e) =>
            {
                e.CommandLine.AppendSwitchWithValue("allow-running-insecure-content", "1");
                e.CommandLine.AppendSwitchWithValue("enable-media-stream", "1");
            };

            ChromiumWebBrowser.OnBeforeCfxInitialize += ChromiumWebBrowser_OnBeforeCfxInitialize;
            ChromiumWebBrowser.Initialize();
            InitializeComponent();
        }

        private void ChromiumWebBrowser_OnBeforeCfxInitialize(Chromium.WebBrowser.Event.OnBeforeCfxInitializeEventArgs e)
        {
            e.Settings.CachePath = Path.Combine(Path.GetTempPath(), "SoftphoneBrowserCache");
        }
    }
}
