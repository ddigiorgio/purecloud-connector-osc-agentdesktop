﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.AddIn;
using RightNow.AddIns.AddInViews;

namespace Softphone.PureCloud.Connector.OSC.Scripts
{
    [AddIn("PureCloudConnectorScript AddIn", Version = "1.0.0.0")]

    public partial class PureCloudConnectorScripts : Panel, IGlobalDockWindowControl
    {
        public PureCloudConnectorScripts()
        {
            
        }

        public void SetGDWContext(IGDWContext GlobalDockableWindowContext)
        {          
        }

        public string GroupName
        {
            get
            {
                return string.Empty;
            }
        }

        public int Order
        {
            get
            {
                return 2;
            }
        }

        public Keys Shortcut
        {
            get
            {
                return Keys.None;
            }
        }

        public void ShortcutActivated()
        {

        }

        public Control GetControl()
        {
            return this;
        }

        public bool Initialize(IGlobalContext GlobalContext)
        {            
            return false;
        }
    }
}
