﻿
function ContactPopUpById(message) {    
    Context.LogMessage("ContactPopUpById calling ...");

    var interactionId = message.InteractionID.Value;
    var contactId = Script.System.Convert.ToInt32(message.attachdata.CONTACT_ID.Value);

    var taskId = CreateWDETask(message.CallType.Value + ' ' + message.MediaType.Value + ' - ' + interactionId,
                                 contactId,
                                 message);
    if (taskId != null) {
        OpenEntity('Contact', contactId);

        var attachData = createMap();
        attachData.Add("CONTACT_ID", Script.System.String.Concat("", contactId));
        attachData.Add("ACTIVITY_ID", Script.System.String.Concat("", taskId));

        SetAttachdata(message, attachData);
    } else
    {
        Context.LogMessage("ContactPopUpById unable to create Task entity record ...");
    }
}


function VoiceInbound(message) {    
    Context.LogMessage("VoiceInbound calling ...");

    if (Message.attachdata.CONTACT_ID != null) {
        if (Message.attachdata.ACTIVITY_ID == null) {
            ContactPopUpById(Message)
            return;
        } else {
            OpenEntity("Task", Message.attachdata.ACTIVITY_ID.Value);
        }
    }

    if (Message.attachdata.ACTIVITY_ID != null) {
        OpenEntity("Task", Message.attachdata.ACTIVITY_ID.Value);
        return;
    }

    ContactPopUpByANI(message);
}

function VoiceOutbound(message) {    
    Context.LogMessage("VoiceOutbound calling ...");

    if (Message.attachdata.CONTACT_ID != null) {
        if (Message.attachdata.ACTIVITY_ID == null) {
            ContactPopUpById(Message)
            return;
        } else {
            OpenEntity("Task", Message.attachdata.ACTIVITY_ID.Value);
        }
    }

    if (Message.attachdata.ACTIVITY_ID != null) {
        OpenEntity("Task", Message.attachdata.ACTIVITY_ID.Value);
        return;
    }

    ContactPopUpByDNIS(message);
}


function MediaInbound(message) {    
    Context.LogMessage("MediaInbound calling ...");

    if (Message.attachdata.CONTACT_ID != null) {
        if (Message.attachdata.ACTIVITY_ID == null) {
            ContactPopUpById(Message)
            return;
        } else {
            OpenEntity("Task", Message.attachdata.ACTIVITY_ID.Value);
        }
    }

    if (Message.attachdata.ACTIVITY_ID != null) {
        OpenEntity("Task", Message.attachdata.ACTIVITY_ID.Value);
        return;
    }

    ContactPopUpByEmail(message);
}

function ContactPopUpByEmail(message) {
   
    Context.LogMessage("ContactPopUpByEmail calling ...");

    var attachData = createMap();

    var contactId = SearchContactByEmail(message.EmailAddress.Value);

    var taskId = CreateWDETask(message.CallType.Value + ' ' + message.MediaType.Value + " - " + message.InteractionID.Value,
                               contactId,
                               message);

    if (taskId == null) {
        Context.LogMessage("ContactPopUpByEmail unable to create Task entity record ...");
        return;
    }

    attachData.Add("ACTIVITY_ID", Script.System.String.Concat("", taskId));

    if (contactId != null) {
        attachData.Add("CONTACT_ID", Script.System.String.Concat("", contactId));

        Context.LogMessage("ContactPopUpByEmail contact found ...");
        OpenEntity('Contact', contactId);
    } else {
        Context.LogMessage("ContactPopUpByEmail single contact not found ...");
        OpenEntity('Task', taskId);
        ShowContactsReportByEmail(message.EmailAddress.Value);
    }

    SetAttachdata(message, attachData);
}

function ContactPopUpByANI(message) {
    
    Context.LogMessage("ContactPopUpByANI calling ...");

    var phoneNumber = message.ANI.Value;	
	Context.LogMessage("ContactPopUpByANI ANI: " + phoneNumber);
	phoneNumber = Script.Softphone.PureCloud.OSC.Utility.StringUtility.Replace(phoneNumber, "tel:", "");
	Context.LogMessage("ContactPopUpByANI realANI: " + phoneNumber);

    var attachData = createMap();

    var contactId = SearchContactByPhone(phoneNumber);

    var taskId = CreateWDETask(message.CallType.Value + ' ' + message.MediaType.Value + " - " + message.InteractionID.Value,
                               contactId,
                               message);

    if (taskId == null)
    {
        Context.LogMessage("ContactPopUpByANI unable to create Task entity record ...");
        return;
    }

    attachData.Add("ACTIVITY_ID", Script.System.String.Concat("", taskId));

    if (contactId != null) {
        attachData.Add("CONTACT_ID", Script.System.String.Concat("", contactId));

        Context.LogMessage("ContactPopUpByANI contact found ...");
        OpenEntity('Contact', contactId);
    } else {
        Context.LogMessage("ContactPopUpByANI single contact not found ...");
        OpenEntity('Task', taskId);
        ShowContactsReportByPhone(phoneNumber);
    }

    SetAttachData(message, attachData);
}

function ContactPopUpByDNIS(message) {
    
    Context.LogMessage("ContactPopUpByDNIS calling ...");

    var phoneNumber = message.DNIS.Value;

    var attachData = createMap();

    var contactId = SearchContactByPhone(phoneNumber);

    var taskId = CreateWDETask(message.CallType.Value + ' ' + message.MediaType.Value + " - " + message.InteractionID.Value,
                               contactId,
                               message);

    if (taskId == null) {
        Context.LogMessage("ContactPopUpByDNIS unable to create Task entity record ...");
        return;
    }

    attachData.Add("ACTIVITY_ID", Script.System.String.Concat("", taskId));

    if (contactId != null) {
        attachData.Add("CONTACT_ID", Script.System.String.Concat("", contactId));

        Context.LogMessage("ContactPopUpByDNIS contact found ...");
        OpenEntity('Contact', contactId);
    } else {
        Context.LogMessage("ContactPopUpByDNIS single contact not found ...");
        OpenEntity('Task', taskId);
        ShowContactsReportByPhone(phoneNumber);
    }

    SetAttachData(message, attachData);
}

function ShowContactsReportByPhone(phoneNumber) {

    var ListOfFilter = Script.System.Collections.Generic.List(Script.RightNow.AddIns.AddInViews.IReportFilter2);
    var filterList = new ListOfFilter();

    var filter = OSC.AddInViewsDataFactoryWrapper(Script.RightNow.AddIns.AddInViews.IReportFilter2);

    filter.OperatorType = Script.RightNow.AddIns.AddInViews.ReportFilterOperatorType.Equals;
    filter.Value = phoneNumber;
    filter.Expression = "contacts.any_phone";
    filterList.Add(filter);

    //8014 is the id associated to the standard Coontact Search report
    Context.AutomationContext.RunReport(8014, filterList);
}

function ShowContactsReportByEmail(email) {

    var ListOfFilter = Script.System.Collections.Generic.List(Script.RightNow.AddIns.AddInViews.IReportFilter2);
    var filterList = new ListOfFilter();

    var filter = OSC.AddInViewsDataFactoryWrapper(Script.RightNow.AddIns.AddInViews.IReportFilter2);

    filter.OperatorType = Script.RightNow.AddIns.AddInViews.ReportFilterOperatorType.Equals;
    filter.Value = email;
    filter.Expression = "contacts.any_email";
    filterList.Add(filter);

    //8014 is the id associated to the standard Coontact Search report
    Context.AutomationContext.RunReport(8014, filterList);
}