Context.LogMessage("InteractionContentRequestCalling ...");
Context.LogMessage("InteractionContentRequestCalling .InteractionID: " + Message.InteractionID.Value);

var taskId = SearchTaskByInteractionId(Message.InteractionID.Value);
if (taskId != null)
{
	OpenEntity("Task", taskId);
}