﻿function createMap(type) {
    var Dictionary = Script.System.Collections.Generic.Dictionary;

    if (type) {
        return new Dictionary(Script.System.String, type);
    }

    return new Dictionary(Script.System.String, Script.System.String);
}

function getValueFromMap(map, keyName, type) {
    var myobj = null;
    if (type) {
        myobj = SoftPhoneFunctions.newVar(type);

    } else {
        myobj = SoftPhoneFunctions.newVar(Script.System.String);
    }

    var found = map.TryGetValue(keyName, myobj.out);
    if (found) {
        return myobj.value;
    }
    return null;
}

function OpenEntity(entityType, entitityId) {

    entitityId = Script.System.Convert.ToInt64(entitityId);
    Context.AutomationContext.EditWorkspaceRecord(entityType, entitityId);
}

function GetAcceptedLanguage() {

    switch (Context.LanguageId)
    {
        case 1:
            return "EN-us";
        case 2:
            return "CS-cz";
        case 3:
            return "DE-de";
        case 4:
            return "EL-gr";
        case 5:
            return "EN-gb";
        case 6:
            return "ES-es";
        case 7:
            return "FI-fi";     
        case 9:
            return "FR-fr";
        case 10:
            return "IT-it";
        case 11:
            return "JA-jp";
        case 12:
            return "NL-nl";
        case 13:
            return "PT-br";
        case 14:
            return "SV-se";
        case 15:
            return "PL-pl";    
        case 17:
            return "ZH-CN";
        case 18:
            return "ZH-tw";       
        case 20:
            return "KO-kr";
        case 21:
            return "DA-dk";
        case 22:
            return "NO-no";      
        case 26:
            return "HU-hu";
    }

    return null;
}

function PrintSDKInformation() {    
    Context.LogMessage("API Rest URL: " + GetInterfaceRESTUrl());
    Context.LogMessage("Context Session ID: " + Context.SessionId);
    Context.LogMessage("Context LanguageId: " + Context.LanguageId);
    Context.LogMessage("Context Language LanguageName: " + Context.LanguageName);
    var accept = GetAcceptedLanguage();
    if (accept == null)
        Context.LogMessage("AcceptLanguage header warning");
    else 
        Context.LogMessage("Accept Language header: " + accept);
}

function GetRESTRequestObject(_requestString, method) {    
    var restApiURL = GetInterfaceRESTUrl();
    
    var request = new Script.RestSharp.RestRequest(_requestString, method);

    request.AddHeader("Content-Type", "text/html");
    request.AddHeader("Authorization", "Session " + Context.SessionId);    

    var acceptLanguageHeader = GetAcceptedLanguage();
    if (acceptLanguageHeader != null)
    {
        request.AddHeader("Accept-Language", acceptLanguageHeader);
    }

    return request;
}

function SendRESTRequest(request) {    
    Context.LogMessage("calling SendRESTRequest...");

    var client = new Script.RestSharp.RestClient(GetInterfaceRESTUrl());
    var response = client.Execute(request);
    Context.LogMessage("SendRESTRequest response: " + response.Content);

    var json = Script.Newtonsoft.Json.Linq.JObject.Parse(response.Content);

    var error = OSC.GetValueFromJSON(json, "o:errorCode");
    if (error == null) {
        return json;
    } else
    {
        Context.LogMessage("SendRESTRequest restapi error: " + OSC.GetValueFromJSON(json, "title"));
        return null;
    }
}


function GetInterfaceRESTUrl() {
    var regEx = new Script.System.Text.RegularExpressions.Regex("/");
    var res = regEx.Split(Context.InterfaceURL);

    return res[0] + "/" + res[1] + "/" + res[2] + "/services/rest/connect/v1.3/";
}

function SearchContactByPhone(_phoneNumber)
{    
    Context.LogMessage("SearchContactByPhone calling ...");
    Context.LogMessage("SearchContactByPhone phoneNumber: " + _phoneNumber);
    
    var request = GetRESTRequestObject("queryResults/?query=select id from contacts where phones.number='" + _phoneNumber + "'",
                                       Script.RestSharp.Method.GET);    
    
    var json = SendRESTRequest(request);
    if (json != null)
    {
        var counter = Script.System.Convert.ToInt32(OSC.GetValueFromJSON(json, "items[0].count"));
        Context.LogMessage("SearchContactByPhone contacts found: " + counter);

        if (counter == 1) {
            var contactID = OSC.GetValueFromJSON(json, "items[0].rows[0][0]");
            Context.LogMessage("SearchContactByPhone ContactId found: " + contactID);

            return contactID;
        }
    } 
    
    return null;
}

function SearchContactByEmail(_email) {    
    Context.LogMessage("SearchContactByEmail calling ...");
    Context.LogMessage("SearchContactByEmail email: " + _email);
    
    var request = GetRESTRequestObject("queryResults/?query=select id from contacts where emails.address='" + _email + "'",
                                      Script.RestSharp.Method.GET);
    
    var json = SendRESTRequest(request);
  
    if (json != null) {
        var counter = Script.System.Convert.ToInt32(OSC.GetValueFromJSON(json, "items[0].count"));
        Context.LogMessage("SearchContactByEmail contacts found: " + counter);

        if (counter == 1) {
            var contactID = OSC.GetValueFromJSON(json, "items[0].rows[0][0]");
            Context.LogMessage("SearchContactByEmail ContactId found: " + contactID);

            return contactID;
        }
    } 

    return null;
}

function CreateWDETask(title, contactId, message) {    
    Context.LogMessage("CreateWDETask calling ...");
        
    var data = {};
    data.name = title;

    data.assignedToAccount = {};
    data.assignedToAccount.id = Context.AccountId;

    data.statusWithType = {};
    data.statusWithType.status = {};
    data.statusWithType.status.id = 17;

    if (contactId != null) {
        data.contact = {};
        data.contact.id = Script.System.Convert.ToInt32(contactId);
    }

    data.customFields = {};
    data.customFields.c = {};
    data.customFields.c.iwsinteractionid = message.InteractionID.Value;
    data.customFields.c.iwsmedianame = message.MediaType.Value;
   
    var request = GetRESTRequestObject("tasks",
                                      Script.RestSharp.Method.POST);
    request.AddParameter("application/json", Script.Newtonsoft.Json.JsonConvert.SerializeObject(data), Script.RestSharp.ParameterType.RequestBody);

    var json = SendRESTRequest(request);
    
    if (json != null) {
        var taskId = Script.System.Convert.ToInt32(OSC.GetValueFromJSON(json, "id"));
        Context.LogMessage("CreateWDETask TaskId created: " + taskId);
        return taskId;
    } 

    return null;
}

function SetAttachdata(message, attachdata)
{	
	var messageToSend = createMap(Script.System.Object);
    messageToSend.Add("command","SETATTACHDATA");		
	messageToSend.Add("interactionid", message.InteractionID.Value);		
    messageToSend.Add("attachdata", attachdata);	

	OSC.SendCommand(messageToSend);	
}

function SearchTaskByInteractionId(_interactionId)
{    
    Context.LogMessage("SearchTaskByInteractionId calling ...");
    Context.LogMessage("SearchTaskByInteractionId _interactionId: " + _interactionId);
    
    var request = GetRESTRequestObject("queryResults/?query=select id from tasks where customFields.c.iwsinteractionid='" + _interactionId + "' ORDER BY createdTime DESC",
                                       Script.RestSharp.Method.GET);    
    
    var json = SendRESTRequest(request);
    if (json != null)
    {
        var counter = Script.System.Convert.ToInt32(OSC.GetValueFromJSON(json, "items[0].count"));
        Context.LogMessage("SearchTaskByInteractionId contacts found: " + counter);

        if (counter >= 1) {
            var taskID = OSC.GetValueFromJSON(json, "items[0].rows[0][0]");
            Context.LogMessage("SearchTaskByInteractionId TaskId found: " + taskID);

            return taskID;
        }
    }     
    return null;
}