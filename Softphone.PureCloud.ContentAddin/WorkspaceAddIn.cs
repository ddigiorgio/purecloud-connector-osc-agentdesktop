﻿using System.AddIn;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using RightNow.AddIns.AddInViews;
using RightNow.AddIns.Common;

////////////////////////////////////////////////////////////////////////////////
//
// File: WorkspaceAddIn.cs
//
// Comments:
//
// Notes: 
//
// Pre-Conditions: 
//
////////////////////////////////////////////////////////////////////////////////
namespace Softphone.PureCloud.ContentAddin
{

    public class WorkspaceAddIn : Panel, IWorkspaceComponent2
    {
        private AppControl.ApplicationControl applicationControl1;

        [WorkspaceConfigProperty("https://apps.mypurecloud.com", Description = "PureCloud Url",
                ToolTip = "PureCloud Url",
                NameInDesigner = "PureCloud Url")]
        public string WebAppURL { get; set; }

        [WorkspaceConfigProperty("0", Description = "IWSInteractionID field ID",
                ToolTip = "IWSInteractionID field ID",
                NameInDesigner = "IWSInteractionID field ID")]
        public int IWSInteractionIdFieldID { get; set; }

        /// <summary>
        /// The current workspace record context.
        /// </summary>
        private IRecordContext _recordContext;
        private IGlobalContext _context;
        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="inDesignMode">Flag which indicates if the control is being drawn on the Workspace Designer. (Use this flag to determine if code should perform any logic on the workspace record)</param>
        /// <param name="RecordContext">The current workspace record context.</param>
        public WorkspaceAddIn(bool inDesignMode, IRecordContext RecordContext, IGlobalContext _ctx)
        {
            _recordContext = RecordContext;
            _context = _ctx;

            if (!inDesignMode)
                _recordContext.DataLoaded += _recordContext_DataLoaded;            
        }

        private void _recordContext_DataLoaded(object sender, System.EventArgs e)
        {
            if (this.applicationControl1 == null)
            {

                ITask task = _recordContext.GetWorkspaceRecord(WorkspaceRecordType.Task) as ITask;

                string interactionId = string.Empty;
                
                foreach (var cf in task.CustomField)
                {
                    if (cf.CfId == IWSInteractionIdFieldID)
                        interactionId = "" + cf.ValStr;                    
                }

                if (string.IsNullOrEmpty(interactionId))
                {
                    _context.LogMessage("ERROR: unable to find value for interactionId field");
                    return;
                }

                this.applicationControl1 = new AppControl.ApplicationControl();

                this.applicationControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.applicationControl1.Dock = System.Windows.Forms.DockStyle.Fill;
                this.applicationControl1.Arguments = string.Format("{0} {1}", this.WebAppURL, interactionId);

                Assembly ass = Assembly.GetAssembly(this.GetType());

                string dir = Path.GetDirectoryName(Path.GetDirectoryName(ass.Location));

                this.applicationControl1.EXELocation = Path.Combine(dir,
                    "Softphone.PureCloud.Connector.OSC.AgentDesktop",
                    "ChromiumInteraction.exe");

                this.applicationControl1.Location = new System.Drawing.Point(0, 0);
                this.applicationControl1.Name = "applicationControl1";

                this.applicationControl1.TabIndex = 0;
                this.Controls.Add(this.applicationControl1);
            }
        }

        #region IAddInControl Members

        /// <summary>
        /// Method called by the Add-In framework to retrieve the control.
        /// </summary>
        /// <returns>The control, typically 'this'.</returns>
        public Control GetControl()
        {
            return this;
        }

        #endregion

        #region IWorkspaceComponent2 Members

        /// <summary>
        /// Sets the ReadOnly property of this control.
        /// </summary>
        public bool ReadOnly { get; set; }

        /// <summary>
        /// Method which is called when any Workspace Rule Action is invoked.
        /// </summary>
        /// <param name="ActionName">The name of the Workspace Rule Action that was invoked.</param>
        public void RuleActionInvoked(string ActionName)
        {
        }

        /// <summary>
        /// Method which is called when any Workspace Rule Condition is invoked.
        /// </summary>
        /// <param name="ConditionName">The name of the Workspace Rule Condition that was invoked.</param>
        /// <returns>The result of the condition.</returns>
        public string RuleConditionInvoked(string ConditionName)
        {
            return string.Empty;
        }

        #endregion
    }

    [AddIn("Softphone PureCloud Content AddIn", Version = "1.0.0.0")]
    public class WorkspaceAddInFactory : IWorkspaceComponentFactory2
    {
        private IGlobalContext _gContext;
        #region IWorkspaceComponentFactory2 Members

        /// <summary>
        /// Method which is invoked by the AddIn framework when the control is created.
        /// </summary>
        /// <param name="inDesignMode">Flag which indicates if the control is being drawn on the Workspace Designer. (Use this flag to determine if code should perform any logic on the workspace record)</param>
        /// <param name="RecordContext">The current workspace record context.</param>
        /// <returns>The control which implements the IWorkspaceComponent2 interface.</returns>
        public IWorkspaceComponent2 CreateControl(bool inDesignMode, IRecordContext RecordContext)
        {
            return new WorkspaceAddIn(inDesignMode, RecordContext, _gContext);
        }

        #endregion

        #region IFactoryBase Members

        /// <summary>
        /// The 16x16 pixel icon to represent the Add-In in the Ribbon of the Workspace Designer.
        /// </summary>
        public Image Image16
        {
            get { return Properties.Resources.AddIn16; }
        }

        /// <summary>
        /// The text to represent the Add-In in the Ribbon of the Workspace Designer.
        /// </summary>
        public string Text
        {
            get { return "Softphone PureCloud Content AddIn"; }
        }

        /// <summary>
        /// The tooltip displayed when hovering over the Add-In in the Ribbon of the Workspace Designer.
        /// </summary>
        public string Tooltip
        {
            get { return "Softphone PureCloud Content AddIn"; }
        }

        #endregion

        #region IAddInBase Members

        /// <summary>
        /// Method which is invoked from the Add-In framework and is used to programmatically control whether to load the Add-In.
        /// </summary>
        /// <param name="GlobalContext">The Global Context for the Add-In framework.</param>
        /// <returns>If true the Add-In to be loaded, if false the Add-In will not be loaded.</returns>
        public bool Initialize(IGlobalContext GlobalContext)
        {
            _gContext = GlobalContext;
            return true;
        }

        #endregion
    }
}