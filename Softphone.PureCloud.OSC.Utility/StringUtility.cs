﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softphone.PureCloud.OSC.Utility
{
    public static class StringUtility
    {
        public static string Replace(string source, string stringToReplace, string newString)
        {
            return source.Replace(stringToReplace, newString);
        }

        public static int IndexOf(string source, string stringTofind)
        {
            return source.IndexOf(stringTofind);
        }
    }
}
