﻿using Chromium;
using Chromium.Remote;
using Chromium.WebBrowser;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChromiumFXWebBrowser
{
    public partial class Form1 : Form
    {
        private string[] arguments; 
        public Form1(string[] arguments)
        {
            this.arguments = arguments;            

            ChromiumWebBrowser.OnBeforeCommandLineProcessing += (e) =>
            {
                e.CommandLine.AppendSwitchWithValue("allow-running-insecure-content", "1");
                e.CommandLine.AppendSwitchWithValue("enable-media-stream", "1");
                e.CommandLine.AppendSwitchWithValue("disable-web-security", "1");                
            };            
            ChromiumWebBrowser.OnBeforeCfxInitialize += ChromiumWebBrowser_OnBeforeCfxInitialize;
            ChromiumWebBrowser.Initialize();

            InitializeComponent();
        }


        private void ChromiumWebBrowser_OnBeforeCfxInitialize(Chromium.WebBrowser.Event.OnBeforeCfxInitializeEventArgs e)
        {
            e.Settings.CachePath = Path.Combine(Path.GetTempPath(), "SoftphoneBrowserCache");                  
        }

        public string ExecutionPath
        {
            get
            {
                Assembly ass = Assembly.GetAssembly(this.GetType());
                return Path.GetDirectoryName(ass.Location);
            }
        }        
    }
}
