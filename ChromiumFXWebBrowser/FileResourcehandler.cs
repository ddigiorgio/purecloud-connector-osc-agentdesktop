﻿using Chromium;
using Chromium.Event;
using System.IO;
using System.Runtime.InteropServices;

namespace ChromiumFXWebBrowser
{
    class FileResourceHandler : CfxResourceHandler
    {       
        private int bytesDone;            
        private byte[] data;

        public FileResourceHandler(string _filePath)
        {            
            data = File.ReadAllBytes(_filePath);
            this.GetResponseHeaders += new CfxGetResponseHeadersEventHandler(ResourceHandler_GetResponseHeaders);
            this.ProcessRequest += new CfxProcessRequestEventHandler(ResourceHandler_ProcessRequest);
            this.ReadResponse += new CfxReadResponseEventHandler(ResourceHandler_ReadResponse);
        }

        void ResourceHandler_ProcessRequest(object sender, CfxProcessRequestEventArgs e)
        {
            bytesDone = 0;
            e.Callback.Continue();
            e.SetReturnValue(true);
            
        }

        void ResourceHandler_GetResponseHeaders(object sender, CfxGetResponseHeadersEventArgs e)
        {            
            e.ResponseLength = this.data.Length;
            e.Response.MimeType = "text/html";
            e.Response.Status = 200;
            e.Response.StatusText = "OK";
        }

        void ResourceHandler_ReadResponse(object sender, CfxReadResponseEventArgs e)
        {
            int bytesToCopy = this.data.Length - bytesDone;
            if (bytesToCopy > e.BytesToRead)
                bytesToCopy = e.BytesToRead;
            Marshal.Copy(this.data, bytesDone, e.DataOut, bytesToCopy);
            e.BytesRead = bytesToCopy;
            bytesDone += bytesToCopy;
            e.SetReturnValue(true);
        }
    }
}
