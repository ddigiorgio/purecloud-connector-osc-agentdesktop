﻿using Chromium;
using Chromium.Remote;
using Chromium.WebBrowser;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace ChromiumFXWebBrowser
{
    public class CTIMessage
    {
        public string Message { get; set; }

        public string CTIEventName { get; set; }
    }

    

    //Used for WM_COPYDATA for string messages
   

    partial class Form1
    {
        public struct COPYDATASTRUCT
        {
            public IntPtr dwData;
            public int cbData;
            [MarshalAs(UnmanagedType.LPStr)]
            public string lpData;
        }

        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        public static extern int SendMessage(int hWnd, int Msg, int wParam, ref COPYDATASTRUCT lParam);


        public const int WM_COPYDATA = 0x4A;
        private static readonly HttpClient client = new HttpClient();        
        private ChromiumWebBrowser wb;
        //private SimpleHTTPServer _embeddedHttpServer;        
        private Button label;
        private int parentWindowhandle;
        
        private string pureCloudRegion;
        private string clientID;
        private string language;
        private string oscDomain;
       

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            /*if (_embeddedHttpServer != null)
                _embeddedHttpServer.Stop();*/

            base.Dispose(disposing);
        }

        private void ReadArguments()
        {
            if (this.arguments.Length != 5)
                throw new ArgumentException("Expected 5 parameters instead of " + this.arguments.Length);

            this.pureCloudRegion = this.arguments[0];
            this.clientID = this.arguments[1];                        
            this.language = this.arguments[2];
            this.oscDomain = this.arguments[3];
            this.parentWindowhandle = Convert.ToInt32(this.arguments[4]);
        }


        private void StartHTTPServer()
        {
            /*_embeddedHttpServer = new SimpleHTTPServer(ExecutionPath, this.localPort);            
            _embeddedHttpServer.CommandReceived += _embeddedHttpServer_CommandReceived;*/
        }

        /*private void _embeddedHttpServer_CommandReceived(object sender, CommandReceivedEventArgs e)
        {
            this.wb.ExecuteJavascript(string.Format("executeCommand('{0}')", e.Command));
        }*/

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ReadArguments();

            var scheme = new CfxSchemeHandlerFactory();
            scheme.Create += Scheme_Create1;

            CfxRuntime.RegisterSchemeHandlerFactory("https", this.oscDomain, scheme);
            StartHTTPServer();

            this.components = new System.ComponentModel.Container();
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);

            /*this.label = new Button();
            this.label.Click += Label_Click;*/
            //var url = string.Format("http://localhost:{0}/CEF.html?pureDomain={1}&language={2}", this.localPort, this.pureCloudURL, this.language);
            var url = string.Format("https://{0}/CEF.html?pureDomain={1}&language={2}&clientid={3}", this.oscDomain, this.pureCloudRegion, this.language, this.clientID);
                
            //var url = "http://localhost:8088/CEF.html";



            wb = new ChromiumWebBrowser(url);
            
            wb.MinimumSize = new System.Drawing.Size(250, 400);
            
            wb.RequestHandler.CanSetCookie += (s, e) =>
            {
                e.SetReturnValue(true);
            };

            wb.RequestHandler.CanGetCookies += (s, e) =>
            {
                e.SetReturnValue(true);
            };

            wb.GlobalObject.AddFunction("ctiEventReceived").Execute += Form1_Execute;
            wb.Dock = DockStyle.Fill;
                    
            wb.LifeSpanHandler.OnBeforePopup += LifeSpanHandler_OnBeforePopup;            

            this.Controls.Add(wb);
            
        }

        private void Scheme_Create1(object sender, Chromium.Event.CfxCreateEventArgs e)
        {
            if (e.SchemeName == "https" && e.Request.Url.Contains("CEF.html"))
            {
                string assembliesPath = System.IO.Path.GetDirectoryName(new System.Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath);
                var resourcehandler = new FileResourceHandler(Path.Combine(assembliesPath, "CEF.html"));
                e.SetReturnValue(resourcehandler);
            }
        }

        private void Label_Click(object sender, EventArgs e)
        {
            /*CfxWindowInfo windowInfo = new CfxWindowInfo();

            windowInfo.Style = WindowStyle.WS_OVERLAPPEDWINDOW | WindowStyle.WS_CLIPCHILDREN | WindowStyle.WS_CLIPSIBLINGS | WindowStyle.WS_VISIBLE;
            windowInfo.ParentWindow = IntPtr.Zero;
            windowInfo.WindowName = "Dev Tools";
            windowInfo.X = 200;
            windowInfo.Y = 200;
            windowInfo.Width = 800;
            windowInfo.Height = 600;

            wb.BrowserHost.ShowDevTools(windowInfo, new CfxClient(), new CfxBrowserSettings(), null);*/

            wb.EvaluateJavascript("getSelectedInteractionID()", (value, exception) => {
                if (exception != null)
                {
                    var message = exception.Message;
                }
                if (value != null)
                {
                    var s = value.StringValue;
                }
            });

        }

        private void LifeSpanHandler_OnBeforePopup(object sender, Chromium.Event.CfxOnBeforePopupEventArgs e)
        {
            e.SetReturnValue(false);

            /*if (!this.interactionByPopup)
            {
                if (e.TargetDisposition == CfxWindowOpenDisposition.NewPopup &&
                    e.TargetFrameName == "scripterOrInteraction")
                {
                    e.SetReturnValue(true);

                    wb.EvaluateJavascript("getSelectedInteractionID()", (value, exception) =>
                    {
                        if (exception != null)
                        {
                            var message = exception.Message;
                        }
                        if (value != null)
                        {
                            string interctionID = value.StringValue;
                            if (!string.IsNullOrEmpty(interctionID))
                            {
                                SendCTIMessage(new CTIMessage()
                                {
                                    CTIEventName = "InteractionContentRequest",
                                    Message = string.Format("{{ 'InteractionID' : '{0}' }}", interctionID)
                                });
                            }
                        }
                    });

                }
                else
                {
                    e.SetReturnValue(false);
                }
            } else
            {
                e.SetReturnValue(false);
            }*/
        }
        

        private void Form1_Execute(object sender, Chromium.Remote.Event.CfrV8HandlerExecuteEventArgs e)
        {
            var numberOfArguments = e.Arguments.Length;
            if (numberOfArguments > 0)
            {
                var firstArg = e.Arguments[0].StringValue;
                var message = JObject.Parse(firstArg);
                if (message["EVENT"] != null)
                {
                    string _event = message["EVENT"].Value<string>();
                    if (!_event.StartsWith("SwitchInteraction"))
                    {
                        if (message["CallType"] != null)
                        {
                            _event += message["CallType"].Value<string>();
                        }
                    }

                    if (_event != "Generic")
                    {
                        SendCTIMessage(new CTIMessage() { CTIEventName = _event, Message = firstArg });
                    }
                }
            }
        }

        private void SendCTIMessage(CTIMessage message)
        {
            string msg = JsonConvert.SerializeObject(message); 
            byte[] sarr = System.Text.Encoding.Default.GetBytes(msg);
            int len = sarr.Length;
            COPYDATASTRUCT cds;
            cds.dwData = (IntPtr)100;
            cds.lpData = msg;
            cds.cbData = len + 1;
            SendMessage(this.parentWindowhandle, WM_COPYDATA, 0, ref cds);
        }


        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_COPYDATA)
            {
                COPYDATASTRUCT mystr = new COPYDATASTRUCT();
                Type mytype = mystr.GetType();
                mystr = (COPYDATASTRUCT)m.GetLParam(mytype);
                this.wb.ExecuteJavascript(string.Format("executeCommand('{0}')", mystr.lpData));
            }
            else
            {
                base.WndProc(ref m);
            }
        }

        #endregion
    }
}

