﻿using AppControl;
using RightNow.AddIns.AddInViews;
using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace Softphone.PureCloud.ContactMakeCallAddIn
{
    public partial class MakeCallForm : Form
    {
        private IContact contact;

        public MakeCallForm(IContact _ctx, IGlobalContext _context)
        {
            this.contact = _ctx;
            InitializeComponent();
            LoadItem();
        }

        private void LoadItem()
        {
            if (!string.IsNullOrEmpty(this.contact.PhHome))
                this.comboBox1.Items.Add("Home Phone: " + this.contact.PhHome);

            if (!string.IsNullOrEmpty(this.contact.PhAsst))
                this.comboBox1.Items.Add("Assistant Phone: " + this.contact.PhAsst);

            if (!string.IsNullOrEmpty(this.contact.PhFax))
                this.comboBox1.Items.Add("Fax Phone: " + this.contact.PhFax);

            if (!string.IsNullOrEmpty(this.contact.PhMobile))
                this.comboBox1.Items.Add("Mobile Phone: " + this.contact.PhMobile);

            if (!string.IsNullOrEmpty(this.contact.PhOffice))
                this.comboBox1.Items.Add("Office Phone: " + this.contact.PhOffice);

            this.comboBox1.SelectedIndex = 0;            
        }        

        private void button1_Click(object sender, EventArgs e)
        {
            string[] text = this.comboBox1.SelectedItem.ToString().Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            if (text.Length == 2)
            {
                var phoneNumber = text[1].Trim();

                IDictionary<string, object> map = new Dictionary<string, object>();
                IDictionary<string, string> attributes = new Dictionary<string, string>();
                attributes.Add("CONTACT_ID", "" + this.contact.ID);

                map.Add("command", "CLICKTODIAL");
                map.Add("number", phoneNumber);
                map.Add("type", "call");
                map.Add("autoPlace", true);
                map.Add("attributes", attributes);

                var appControl = PureCloudConnectorFacade.Instance;
                if (appControl != null)
                {                                      
                    var commandString = new JavaScriptSerializer().Serialize(map);
                    appControl.sendMessageToWidget(commandString);
                    this.Close();
                }               
            }            
        }
    }
}
