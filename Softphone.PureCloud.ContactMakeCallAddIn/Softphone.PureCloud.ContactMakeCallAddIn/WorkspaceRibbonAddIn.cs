﻿using System.AddIn;
using System.Windows.Forms;
using AppControl;
using RightNow.AddIns.AddInViews;

////////////////////////////////////////////////////////////////////////////////
//
// File: WorkspaceRibbonAddIn.cs
//
// Comments:
//
// Notes: 
//
// Pre-Conditions: 
//
////////////////////////////////////////////////////////////////////////////////
namespace Softphone.PureCloud.ContactMakeCallAddIn
{
    public class WorkspaceRibbonAddIn : Panel, IWorkspaceRibbonButton
    {
        /// <summary>
        /// The current workspace record context.
        /// </summary>
        private IRecordContext RecordContext { get; set; }
        private IGlobalContext GlobalContext { get; set; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="inDesignMode">Flag which indicates if the control is being drawn on the Workspace designer.</param>
        /// <param name="RecordContext">The current workspace record context.</param>
        public WorkspaceRibbonAddIn(bool inDesignMode, IRecordContext RecordContext, IGlobalContext _ctx)
        {
            this.RecordContext = RecordContext;
            GlobalContext = _ctx;
        }

        #region IWorkspaceRibbonButton Members

        /// <summary>
        /// This method is invoked when the ribbon button is clicked.
        /// </summary>
        public new void Click()
        {
            var pureConnector = PureCloudConnectorFacade.Instance;
            if (pureConnector != null)
            {
                IContact contact = this.RecordContext.GetWorkspaceRecord(RightNow.AddIns.Common.WorkspaceRecordType.Contact) as IContact;
                if (NoNumberValorized(contact))
                {
                    MessageBox.Show("All phone numbers for this contact are not set.", "Softphone PureCloud Connector Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    using (MakeCallForm _form = new MakeCallForm(contact, GlobalContext))
                    {
                        _form.StartPosition = FormStartPosition.CenterScreen;
                        _form.ShowDialog();
                    }
                }
                
            } else
            {
                MessageBox.Show("Unable to find a valid PureCloudConnector instance", "Softphone PureCloud Connector Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                GlobalContext.LogMessage("Unable to find a valid Softphone PureCloud Connector instance");
            }
        }

        private bool NoNumberValorized(IContact contact)
        {
            return (string.IsNullOrEmpty(contact.PhHome) &&
                   string.IsNullOrEmpty(contact.PhAsst) &&
                   string.IsNullOrEmpty(contact.PhFax) &&
                   string.IsNullOrEmpty(contact.PhMobile) &&
                   string.IsNullOrEmpty(contact.PhOffice));
        }

        #endregion
    }

    [AddIn("Contact MakeCall AddIn", Version = "1.0.0.0")]
    public class WorkspaceRibbonButtonFactory : IWorkspaceRibbonButtonFactory
    {
        private IGlobalContext Context;
        #region IWorkspaceRibbonButtonFactory Members

        /// <summary>
        /// Method which is invoked by the AddIn framework when the control is created.
        /// </summary>
        /// <param name="inDesignMode">Flag which indicates if the control is being drawn on the Workspace Designer. (Use this flag to determine if code should perform any logic on the workspace record)</param>
        /// <param name="RecordContext">The current workspace record context.</param>
        /// <returns>The control which implements the IWorkspaceComponent2 interface.</returns>
        public IWorkspaceRibbonButton CreateControl(bool inDesignMode, IRecordContext RecordContext)
        {
            return new WorkspaceRibbonAddIn(inDesignMode, RecordContext, this.Context);
        }

        /// <summary>
        /// The 32x32 pixel icon displayed in the Workspace Ribbon.
        /// </summary>
        public System.Drawing.Image Image32
        {
            get { return Properties.Resources.call32; }
        }

        #endregion

        #region IFactoryBase Members

        /// <summary>
        /// The 16x16 pixel icon to represent the Add-In in the Ribbon of the Workspace Designer.
        /// </summary>
        public System.Drawing.Image Image16
        {
            get { return Properties.Resources.call16; }
        }

        /// <summary>
        /// The text to represent the Add-In in the Ribbon of the Workspace Designer.
        /// </summary>
        public string Text
        {
            get { return "Call"; }
        }

        /// <summary>
        /// The tooltip displayed when hovering over the Add-In in the Ribbon of the Workspace Designer.
        /// </summary>
        public string Tooltip
        {
            get { return "Call contact selecting one of phone number fields provided in the form."; }
        }

        #endregion

        #region IAddInBase Members

        /// <summary>
        /// Method which is invoked from the Add-In framework and is used to programmatically control whether to load the Add-In.
        /// </summary>
        /// <param name="GlobalContext">The Global Context for the Add-In framework.</param>
        /// <returns>If true the Add-In to be loaded, if false the Add-In will not be loaded.</returns>
        public bool Initialize(IGlobalContext GlobalContext)
        {
            Context = GlobalContext;
            return true;
        }

        #endregion
    }
}