﻿
using AppControl;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SoftPhone.WDE.ScriptEngine;
using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace Softphone.Connector.OSC.AgentDesktop
{
    partial class PureCloudConnector
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private AppControl.ApplicationControl applicationControl1;


        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            this.applicationControl1.Close();
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        /// 


        private void InitializeComponent()
        {
            this.applicationControl1 = PureCloudConnectorFacade.Instance; //new AppControl.ApplicationControl();

            Uri myUri = new Uri(this.context.InterfaceURL);
            string host = myUri.Host;  // host is "www.contoso.com"

            components = new System.ComponentModel.Container();            
            this.applicationControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.applicationControl1.Dock = System.Windows.Forms.DockStyle.Fill;            
            this.applicationControl1.Arguments = string.Format("{0} {1} {2} {3}",               
                this.pureCloudRegion,
                this.clientID,
                this.language,
                host);

            this.applicationControl1.CopiedStringEvent += ApplicationControl1_CopiedStringEvent; 

            Assembly ass = Assembly.GetAssembly(this.GetType());

            string dir = Path.GetDirectoryName(ass.Location);
            this.applicationControl1.EXELocation = Path.Combine(dir, "ChromiumFXWebBrowser.exe");

            this.applicationControl1.Location = new System.Drawing.Point(0, 0);
            this.applicationControl1.Name = "applicationControl1";
            
            this.applicationControl1.TabIndex = 0;
            this.Controls.Add(this.applicationControl1);

            this.Dock = DockStyle.Fill;
            this.ResumeLayout();
            this.PerformLayout();
        }

        public void SendCommand(object commandObj)
        {
            string command = JsonConvert.SerializeObject(commandObj);
            this.applicationControl1.sendMessageToWidget(command);
        }

        private void ApplicationControl1_CopiedStringEvent(object sender, AppControl.CopiedStringEventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new EventHandler<AppControl.CopiedStringEventArgs>(this.ApplicationControl1_CopiedStringEvent), sender, e);
            }
            else
            {
                JObject postObject = (JObject)JsonConvert.DeserializeObject(e.CopiedString, typeof(JObject));
                string EventName = postObject["CTIEventName"].ToString();
                var EventMessageObject = JsonConvert.DeserializeObject(postObject["Message"].ToString(), typeof(JObject));
                string EventMessageString = postObject["Message"].ToString();

                switch (EventName)
                {
                    case "ACTIVATESESSION":
                        context.LogMessage(string.Format("PURE incoming event [{0}] with message [{1}]", "ActivateSession", EventMessageString));
                        break;
                    default:
                        context.LogMessage(string.Format("PURE incoming event [{0}] with message [{1}]", EventName, EventMessageString));
                        break;
                }

                try
                {

                    using (IWDEScriptEngine engine = WDEScriptEngine.GetInstance(ScriptDirectory))
                    {
                        engine.AddInstanceObject("Context", context);
                        engine.AddInstanceObject("OSC", this);

                        engine.RunScript("on" + EventName, EventMessageObject);
                    }
                }
                catch (Exception ex)
                {
                    context.LogMessage("[Connector_HandlingCTIEvent] Softphone Script Engine Error: " + FormatException(ex));
                }
            }
        }

        #endregion
    }
}