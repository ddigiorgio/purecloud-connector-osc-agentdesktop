﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RightNow.AddIns.AddInViews;
using System.AddIn;
//using Softphone.PureCloud.Connector.Core;
using System.Reflection;
using System.IO;
using SoftPhone.WDE.ScriptEngine;
using Newtonsoft.Json.Linq;

namespace Softphone.Connector.OSC.AgentDesktop
{
    [AddIn("PureCloudConnector AddIn", Version="1.0.0.0")]

    public partial class PureCloudConnector : Panel, IGlobalDockWindowControl
    {
        private IGDWContext _GlobalDockableWindowContext;
        private IGlobalContext context;
        

        #region Config Properties Addin

        private string pureCloudRegion;
        [ServerConfigProperty(DefaultValue = "mypurecloud.com")]
        public String PureCloudRegion
        {
            get
            {
                return pureCloudRegion;
            }

            set
            {
                pureCloudRegion = value;
            }
        }


        private string clientID;
        [ServerConfigProperty(DefaultValue = "d0d2422c-8767-4bdd-9bd4-697965ea0bdd")]
        public String ClientID
        {
            get
            {
                return clientID;
            }

            set
            {
                clientID = value;
            }
        }


        private string language;
        [ServerConfigProperty(DefaultValue = "en")]
        public String Language
        {
            get
            {
                return language;
            }

            set
            {
                language = value;
            }
        }
        #endregion

        public PureCloudConnector()
        {
            
        }

        public string GroupName
        {
            get
            {
                return "Softphone PureCloud Connector";
            }
        } 

        public int Order
        {
            get
            {
                return 0;
            }
        }

        private string ScriptDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                string parentDirectory = Directory.GetParent(Path.GetDirectoryName(path)).FullName;

                string scriptDir = Path.Combine(parentDirectory, "Softphone.PureCloud.Connector.OSC.Scripts");

                if (Directory.Exists(scriptDir))
                {
                    return scriptDir;
                }
                else
                {
                   context.LogMessage("Unable to find script directory: " + scriptDir);
                }

                return null;
            }
        }

        public void SetGDWContext(IGDWContext GlobalDockableWindowContext)
        {
            _GlobalDockableWindowContext = GlobalDockableWindowContext;
            if (string.IsNullOrEmpty(_GlobalDockableWindowContext.Title))
                _GlobalDockableWindowContext.Title = "Softphone PureCloud Connector";

            _GlobalDockableWindowContext.Docking = RightNow.AddIns.Common.DockingType.Floating;
        }

        public Keys Shortcut
        {
            get
            {
                return Keys.None;
            }
        }

        public void ShortcutActivated()
        {

        }

        public Control GetControl()
        {
            return this;
        }

        private void InitPureCloudConnector()
        {
            /*connector = PureCloudConnectorFacade.Connector;

            connector.Init(Convert.ToInt32(this.senderPort), Convert.ToInt32(this.receiverPort));

            connector.IncomingCTIEvent += Connector_IncomingCTIEvent;
            connector.ActivateSession += Connector_ActivateSession;
            connector.Start();*/
        }

        private void RenameDll(string addInPath, string oldName, string newname)
        {
            string fileName = Path.Combine(addInPath, oldName);
            if (File.Exists(fileName))
            {
                File.Move(fileName, Path.Combine(addInPath, newname));
            }
        }

        private void RenameDLLs()
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            string addInPath = Path.GetDirectoryName(path);

            RenameDll(addInPath, "chrome_elf.dll2", "chrome_elf.dll");
            RenameDll(addInPath, "libcef.dll2", "libcef.dll");
            RenameDll(addInPath, "libcfx.dll2", "libcfx.dll");

            string logFilePath = Path.Combine(addInPath, "debug.log");
            try
            {
                if (File.Exists(logFilePath))
                    File.Delete(logFilePath);
            }catch(Exception ex)
            {
                this.context.LogMessage("Error deleting debug.log file: " + ex.Message);
            }
        }

        /*private void Connector_ActivateSession(object sender, PureCloud.Connector.Core.Model.PureCloudEventIncomingArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new EventHandler<PureCloud.Connector.Core.Model.PureCloudEventIncomingArgs>(this.Connector_ActivateSession), sender, e);
            } else
            {
                context.LogMessage(string.Format("PURE incoming event [{0}] with message [{1}]", "ActivateSession", e.EventMessageString));

                try
                {
                    this.connector.AddObject("user", e.EventMessageObject.user);
                    this.connector.AddObject("pureclouddata", e.EventMessageObject.PureCloudData);
                    
                    using (IWDEScriptEngine engine = WDEScriptEngine.GetInstance(ScriptDirectory))
                    {
                        engine.AddInstanceObject("Context", context);
                        engine.AddInstanceObject("Connector", this.connector);
                        engine.AddInstanceObject("OSC", this);

                        engine.RunScript("onActivateSession", e.EventMessageObject);
                    }
                }catch(Exception ex)
                {
                    context.LogMessage("[Connector_ActivateSession] Softphone Script Engine Error: " + FormatException(ex));
                }
            }            
        }*/

        private static string FormatException(Exception ex)
        {
            StringBuilder result = new StringBuilder();

            while (ex != null)
            {
                result.AppendFormat("MESSAGE: [{0}] - STACKTRACE: [{1}]", ex.Message, ex.StackTrace);
                result.AppendLine();
                ex = ex.InnerException;
            }

            return result.ToString();
        }

        /*private void Connector_IncomingCTIEvent(object sender, PureCloud.Connector.Core.Model.PureCloudEventIncomingArgs e)
        {            
            if (this.InvokeRequired)
            {
                this.Invoke(new EventHandler<PureCloud.Connector.Core.Model.PureCloudEventIncomingArgs>(this.Connector_IncomingCTIEvent), sender, e);
            }
            else
            {                
                context.LogMessage(string.Format("PURE incoming event [{0}] with message [{1}]", e.EventName, e.EventMessageString));

                try
                {
                  
                    using (IWDEScriptEngine engine = WDEScriptEngine.GetInstance(ScriptDirectory))
                    {
                        engine.AddInstanceObject("Context", context);
                        engine.AddInstanceObject("Connector", this.connector);
                        engine.AddInstanceObject("OSC", this);

                        engine.RunScript("on" + e.EventName, e.EventMessageObject);
                    }
                }catch(Exception ex)
                {
                    context.LogMessage("[Connector_IncomingCTIEvent] Softphone Script Engine Error: " + FormatException(ex));
                }
            }
        }*/

        public bool Initialize(IGlobalContext GlobalContext)
        {
            this.context = GlobalContext;
            RenameDLLs();
            InitializeComponent();
            InitPureCloudConnector();
            this.applicationControl1.Init();            
            return true;
        }


        public void EvaluateObject(object _object)
        {
            int f = 0;
        }


        public void SetObjectValue(dynamic _object, string fieldName, string fieldValue)
        {
            _object[fieldName] = fieldValue;
        }

        public T AddInViewsDataFactoryWrapper<T>()
        {
            return AddInViewsDataFactory.Create<T>();
        }


        public object GetValueFromJSON(JObject json, string path)
        {
            dynamic result = json.SelectToken(path);
            if (result != null)
                return result.Value;

            return null;
        }
    }
}
